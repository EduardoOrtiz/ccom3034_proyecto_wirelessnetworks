/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/
#ifndef WIRELESSNETWORK_H
#define WIRELESSNETWORK_H	
#include "myGraph.h"

class wirelessNetwork : public myGraph
{
	protected:
	
	void pathRouting(vector<bool>&, vector<int>&, int, int, bool&);

	public:
	wirelessNetwork();
	wirelessNetwork(double dimension, unsigned int amount);
	void topologyControl();
	vector<int> compassRouting(int from, int to);
};

#endif