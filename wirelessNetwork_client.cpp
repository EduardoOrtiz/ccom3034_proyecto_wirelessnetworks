/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#include "wirelessNetwork.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
using namespace std;

void testTopology(unsigned int n, bool);
void testCompassRouting(unsigned int n);

int main()
{
    for(int i = 500; i < 1001; i+=50)
		testTopology(i, false);

    testCompassRouting(1000);

	return 0;
}

void testTopology(unsigned int n, bool wantMuhGraph)
{
	wirelessNetwork Network(10, n);

	cout << "Current stats:" << endl
		 << "Connected components:" << Network.getConnections() << endl
		 << "Edges:" << Network.getEdges() << endl
		 << "Min degree:" << Network.getMinDegree() << endl
		 << "Average degree:" << Network.getAverageDegree() << endl
		 << "Max degree:" << Network.getMaxDegree() << endl << endl;

	if(wantMuhGraph){
		Network.makeText_graphviz_Graph();
		Network.getStats();
	}

	cout << endl << "Running topologyControl..." << endl;

	Network.topologyControl();

	cout << "Done." << endl << endl
		 << "Stats are now:" << endl
		<< "Connected components:" << Network.getConnections() << endl
		 << "Edges:" << Network.getEdges() << endl
		 << "Min degree:" << Network.getMinDegree() << endl
		 << "Average degree:" << Network.getAverageDegree() << endl
		 << "Max degree:" << Network.getMaxDegree() << endl << endl;

	if(wantMuhGraph){
		Network.makeText_graphviz_Graph();
		Network.getStats();
	}
}

void testCompassRouting(unsigned int n)
{
	wirelessNetwork Network(10, n);
	vector<int> Start, Destination, Path;
	srand(time(NULL));

	for(int c = 0; c < 10; c++){
		Start.push_back(rand() % n);
		Destination.push_back(rand() % n);

		cout << "Returning a path from " << Start[c] << " to "
			 << Destination[c] << "..." << endl;

		Path = Network.compassRouting(Start[c], Destination[c]);

		cout << "Done." << endl;

		if(Path.size() == 0)
			cout << "No path found ;_;";

		else{
			for(int k = 0; k < Path.size() - 1; k++)
				cout << Path[k] << "->";

			cout << Path[Path.size() - 1] << ";";
		}

		cout << endl << endl;
	}

	Network.makeText_graphviz_Graph();

	cout << "Now running topologyControl..." << endl;

	Network.topologyControl();

	cout << "Done." << endl << endl;

	for(int c = 0; c < 10; c++){
		cout << "Returning a path from " << Start[c] << " to "
			 << Destination[c] << "..." << endl;

		Path = Network.compassRouting(Start[c], Destination[c]);

		cout << "Done." << endl;

		if(Path.size() == 0)
			cout << "No path found ;_;";

		else{
			for(int k = 0; k < Path.size() - 1; k++)
				cout << Path[k] << "->";

			cout << Path[Path.size() - 1] << ";";
		}

		cout << endl << endl;
	}

	Network.makeText_graphviz_Graph();
}
