/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#include "myGraph.h"
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>

void Vertex::deleteByIndex(int to)
{
	vector<VNode>::iterator it = this->adjacencyList.begin();

	while(it != adjacencyList.end()){
		if(it->index == to)
			break;

		it++;
	}

	this->adjacencyList.erase(it);
}

double Vertex::getWeightByIndex(int to)
{
	vector<VNode>::iterator it = this->adjacencyList.begin();

	while(it != adjacencyList.end()){
		if(it->index == to)
			break;

		it++;
	}

	return it->weight;
}

template <typename T>
string to_string(T Number)
{
	stringstream ss;
    ss << Number;
    return ss.str();
}

void myGraph::checkConnections(int start, vector<bool>& visited, int& amount)
{
	if(!visited[start]) amount++;

	visited[start] = true;

	for(vector<VNode>::iterator it = Vertices[start].adjacencyList.begin();
	it != Vertices[start].adjacencyList.end(); it++)
		if(!visited[it->index]){
			visited[it->index] = true;
			checkConnections(it->index, visited, amount);
		};
}

vector<unsigned int> myGraph::commonNeighbors
(unsigned int from, unsigned int to)
{
	vector<unsigned int> Neighbors;

	if((from < 0) or (from >= Vertices.size()) or
	(to < 0) or (to >= Vertices.size())) return Neighbors;

	for(unsigned int i = 0; i < Vertices[from].adjacencyList.size(); i++)
		for(unsigned int j = 0; j < Vertices[to].adjacencyList.size(); j++)
			if(Vertices[from].adjacencyList[i].index ==
			   Vertices[to].adjacencyList[j].index)
				Neighbors.push_back(Vertices[from].adjacencyList[i].index);

	return Neighbors;
}

void myGraph::insertVertex(double d, double x, double y)
{
	Vertices.push_back(Vertex(d, x, y));
}

void myGraph::insertVertex(double d)
{
	insertVertex(d, 0, 0);
}

void myGraph::insertEdge(unsigned int from, unsigned int to, double w)
{
	if((from < 0) or (from >= Vertices.size()) or
	(to < 0) or (to >= Vertices.size())) return;

	if(to != from){
		Vertices[from].adjacencyList.push_back(VNode(to, w));
		Vertices[to].adjacencyList.push_back(VNode(from, w));
	}
}

void myGraph::insertEdge(unsigned int from, unsigned int to)
{
	insertEdge(from, to, 0);
}

void myGraph::deleteEdge(unsigned int from, unsigned int to)
{
	for(vector<VNode>::iterator from_it = Vertices[from].adjacencyList.begin();
	from_it != Vertices[from].adjacencyList.end(); from_it++)
		if(from_it->index == to){
			Vertices[from].adjacencyList.erase(from_it);
			break;
		};

	for(vector<VNode>::iterator to_it = Vertices[to].adjacencyList.begin();
	to_it != Vertices[to].adjacencyList.end(); to_it++)
		if(to_it->index == from){
			Vertices[to].adjacencyList.erase(to_it);
			break;
		};
}

int myGraph::getConnections()
{
	vector<bool> visited(Vertices.size(), false);
	int connections = 0;
	for(unsigned int start = 0; start < Vertices.size(); start++)
		checkConnections(start, visited, connections);
	return connections;
}

unsigned int myGraph::getEdges()
{
	unsigned int accum = 0;

	for(unsigned int c = 0; c < Vertices.size(); c++)
		accum += Vertices[c].adjacencyList.size();

	return accum / 2;
}

unsigned int myGraph::getMaxDegree()
{
	if(Vertices.size() == 0) return -1;

	unsigned int max = Vertices[0].adjacencyList.size();

	for(unsigned int c = 1; c < Vertices.size(); c++)
		if(max < Vertices[c].adjacencyList.size())
			max = Vertices[c].adjacencyList.size();

	return max;
}

unsigned int myGraph::getMinDegree()
{
	if(Vertices.size() == 0) return -1;

	unsigned int min = Vertices[0].adjacencyList.size();

	for(unsigned int c = 1; c < Vertices.size(); c++)
		if(min > Vertices[c].adjacencyList.size())
			min = Vertices[c].adjacencyList.size();

	return min;
}

unsigned int myGraph::getAverageDegree()
{
	if(Vertices.size() == 0) return -1;

	unsigned int sumDegree = 0;

	for(unsigned int c = 0; c < Vertices.size(); c++)
		sumDegree += Vertices[c].adjacencyList.size();

	return (sumDegree / Vertices.size());
}

void myGraph::makeText_graphviz()
{
	ofstream graphviz;
	string filename = "myGraph_";

	filename += ("Vertices(" + to_string(Vertices.size()) + ")_");
	filename += ("Edges(" + to_string(getEdges()) + ")_");
	filename += (to_string(time(NULL)) + ".txt");

	graphviz.open(filename.c_str());

	if(!graphviz) return;

	graphviz << "Digraph D {" << endl;

	for(unsigned int i = 0; i < Vertices.size(); i++)
		for(unsigned int j = 0; j < Vertices[i].adjacencyList.size(); j++)
			graphviz << i << " -> " << Vertices[i].adjacencyList[j].index
				  << ";" << endl;

	graphviz << "}";

	graphviz.close();
}

void myGraph::makeText_graphviz_Graph()
{
	ofstream graphviz;
	string filename = "myGraph_";

	filename += ("Vertices(" + to_string(Vertices.size()) + ")_");
	filename += ("Edges(" + to_string(getEdges()) + ")_");
	filename += (to_string(time(NULL)) + ".dot");
	graphviz.open(filename.c_str());

	if(!graphviz) return;

	graphviz << "Graph D {" << endl
			 << "overlap = false;" << endl;
			// << "node [shape=square, fixedsize=false, fontsize=1, width=.10,"
			// << " height=.10];" << endl;

	for(unsigned int c = 0; c < Vertices.size(); c++)
		graphviz << c << " [ pos = \"" << Vertices[c].coor[0] << ","
				 << Vertices[c].coor[1] << "!\"]" << endl;

	for(unsigned int c = 0; c < Vertices.size(); c++)
		for(vector<VNode>::iterator it = Vertices[c].adjacencyList.begin();
			it != Vertices[c].adjacencyList.end(); it++)
			if(c < it->index)
				graphviz << c << "--" << it->index << ";" << endl;

	graphviz << "}" << endl;

	graphviz.close();
}

void myGraph::getStats()
{
	ofstream Stats;
	string filename = "myGraph_";

	filename += ("Vertices(" + to_string(Vertices.size()) + ")_");
	filename += ("Edges(" + to_string(getEdges()) + ")_");
	filename += to_string(time(NULL));

	Stats.open(filename.c_str());

	if(!Stats) return;

	Stats << "Vertices " << Vertices.size() << endl
		  << "Edges " << this->getEdges() << endl
		  << "MaxDegree " << this->getMaxDegree() << endl
		  << "MinDegree " << this->getMinDegree() << endl
		  << "AverageDegree " << this->getAverageDegree();

	Stats.close();
}

