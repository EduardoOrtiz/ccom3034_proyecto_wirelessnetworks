/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/
#include "wirelessNetwork.h"
#include <cstdlib>	//rand()
#include <ctime>	//time
#include <cmath>	//sqrt
#include <iostream>
using namespace std;

wirelessNetwork::wirelessNetwork()
{
	srand(time(NULL));

	for(unsigned int c = 0; c < 500; c++){
		double xcoor = ((double)rand()/(double)RAND_MAX) * 10;
		double ycoor = ((double)rand()/(double)RAND_MAX) * 10;
		insertVertex(((double)rand()/(double)RAND_MAX), xcoor ,ycoor);
	}

	for(unsigned int r = 0; r < 499; r++){
		for(unsigned int s = r + 1; s < 500; s++){
		double distance = sqrt(((Vertices[r].coor[0] - Vertices[s].coor[0])
						* (Vertices[r].coor[0] - Vertices[s].coor[0]))
						+ ((Vertices[r].coor[1] - Vertices[s].coor[1])
						* (Vertices[r].coor[1] - Vertices[s].coor[1])));

			if(distance <= 1.0)
				insertEdge(r, s, distance);
		}
	}
}

wirelessNetwork::wirelessNetwork(double dimension, unsigned int amount)
{
	srand(time(NULL));

	for(unsigned int c = 0; c < amount; c++){
		double xcoor = ((double)rand()/RAND_MAX) * dimension;
		double ycoor = ((double)rand()/RAND_MAX) * dimension;
		insertVertex(((double)rand()/(double)RAND_MAX), xcoor ,ycoor);
	}

	for(unsigned int r = 0; r < (amount - 1); r++){
		for(unsigned int s = r + 1; s < amount; s++){
			//grabs the corresponding x,y coordinates of both vertices and
			//uses the usual metric for two points a,b on R2
			//d(a, b) = |a - b| = sqrt( (ax - bx)^2 + (ay - by)^2)
		double distance = sqrt(((Vertices[r].coor[0] - Vertices[s].coor[0])
						* (Vertices[r].coor[0] - Vertices[s].coor[0]))
						+ ((Vertices[r].coor[1] - Vertices[s].coor[1])
						* (Vertices[r].coor[1] - Vertices[s].coor[1])));

			if(distance <= 1.0)
				insertEdge(r, s, distance);
		}
	}
}

/*
void wirelessNetwork::topologyControl()
{
	//goes through all the vertices
	for(unsigned int c = 0; c < Vertices.size(); c++){
		//for traversing the adjacencyList
		vector<VNode>::iterator it = Vertices[c].adjacencyList.begin();

		//goes through all elements in the List
		while(it != Vertices[c].adjacencyList.end()){
			bool edgeDevastated = false;

			//Intersection between c's adjacencyList and
			//it->index's adjacencyList
			vector<unsigned int> N = commonNeighbors(c, it->index);

			//goes through all common neighbors
			for(unsigned int z = 0; z < N.size(); z++){
				//weight1 = d(c, N[z]), weight2 = d(ptr->index, N[z])
				double weight1, weight2;

				//searches for d(c, N[z])
				for(vector<VNode>::iterator c_it =
				Vertices[c].adjacencyList.begin();
				c_it != Vertices[c].adjacencyList.end(); c_it++)
					if(c_it->index == N[z]){
						weight1 = c_it->weight;
						break; //this for loop
					};

				//searches for d(it->index, N[z])
				for(vector<VNode>::iterator it_it =
				Vertices[it->index].adjacencyList.begin();
				it_it != Vertices[it->index].adjacencyList.end(); it_it++)
					if(it_it->index == N[z]){
						weight2 = it_it->weight;
						break; //this for loop
					};

				//if d(c, N[z]) < d(c, it->index) and
				//d(it->index, N[z]) < d(c, it->index)
				//then delete the edge connecting c, it->index
				if((weight1 < it->weight) and (weight2 < it->weight)){
					deleteEdge(c, it->index);
					edgeDevastated = true;
					break; //for loop with the Neighbors N
				}
			}

			//flag to start traversing the list from the beginning
			if(edgeDevastated)
				it = Vertices[c].adjacencyList.begin();
			else
				it++;
		}
	}
}*/

void wirelessNetwork:: topologyControl()
{
	//for each vertex u in V do
	for(unsigned int c = 0; c < Vertices.size(); c++){
		//temp := N(u)
		vector<VNode> temp = Vertices[c].adjacencyList;
		vector<unsigned int> affectedVertices;

		//for each neighbor v in N(u) do
		for(vector<VNode>::iterator it = Vertices[c].adjacencyList.begin();
			it != Vertices[c].adjacencyList.end(); it++){

			vector<unsigned int> N = commonNeighbors(c, it->index);
			bool edgeDevastated = false;

			for(unsigned int z = 0; z < N.size(); z++){
				double weight1, weight2;

				//searches for d(c, N[z])
				weight1 = Vertices[c].getWeightByIndex(N[z]);

				//searches for d(it->index, N[z])
				weight2 = Vertices[it->index].getWeightByIndex(N[z]);

				//if u and v have a common neighbor w such that
				//|uw| < |uv| and |vw| < |uv| then
				if((weight1 < it->weight) and (weight2 < it->weight))
					for(vector<VNode>::iterator temp_it = temp.begin();
						temp_it != temp.end(); temp_it++)
						if(temp_it->index == it->index){
							//temp := temp - {v}
							temp.erase(temp_it);
							affectedVertices.push_back(it->index);
							edgeDevastated = true;
							break;
						}

				if(edgeDevastated) break;
			}
		}

		//N(u) := temp
		Vertices[c].adjacencyList = temp;

		for(unsigned int s = 0; s < affectedVertices.size(); s++)
			Vertices[affectedVertices[s]].deleteByIndex(c);
	}
}

void wirelessNetwork::pathRouting(vector<bool>& visited,
vector<int>& Traversals, int current, int destination, bool& found)
{
	if(Vertices[current].adjacencyList.size() == 0) return;

	Traversals.push_back(current);

	if(current == destination){
		found = true;
		return;
	}

	if(visited[current]) return;

	visited[current] = true;

	//(name)X corresponds the the x coordinate, similarly with Y
	//CNweight = distance between current and neighbor
	//CDweight = distance between current and destination
	//NDweight = distance between  neighbor and destination
	vector<VNode>::iterator it = Vertices[current].adjacencyList.begin();
	double minAngle = 360.0, desX, desY, curX, curY, neiX, neiY,
		   CNweight, CDweight, NDweight, tempAngle;
	int nextNode = current;

	desX = Vertices[destination].coor[0];
	desY = Vertices[destination].coor[1];
	curX = Vertices[current].coor[0];
	curY = Vertices[current].coor[1];
	CDweight = sqrt(((curX - desX) * (curX - desX)) +
				    ((curY - desY) * (curY - desY)));

	while(it != Vertices[current].adjacencyList.end()){

		if(visited[it->index]){
			it++;
			continue;
		}

		neiX = Vertices[it->index].coor[0];
		neiY = Vertices[it->index].coor[1];

		CNweight = it->weight;
		NDweight = sqrt(((neiX - desX) * (neiX - desX)) +
			            ((neiY - desY) * (neiY - desY)));

		tempAngle = acos(((CDweight * CDweight) + (CNweight * CNweight) -
					      (NDweight * NDweight)) / (2 * CDweight * CNweight));

		if(tempAngle < minAngle){
			minAngle = tempAngle;
			nextNode = it->index;
		}

		it++;
	}

	pathRouting(visited, Traversals, nextNode, destination, found);
}

vector<int> wirelessNetwork::compassRouting(int from, int to)
{
	vector<bool> visited(Vertices.size(), false);
	vector<int> Path;
	vector<int> Insurance;
	bool found = false;
	pathRouting(visited, Path, from, to, found);

	if(found)
		return Path;

	else
		return Insurance;
}
