/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#ifndef MYGRAPH_H
#define MYGRAPH_H

#include <vector>
using namespace std;

//vector Node. Basically a condensed version of Vertex
//used for the adjacencyList
class VNode
{
	public:

	unsigned int index;
	double weight;

	VNode(unsigned int i = 0, double w = 0.0)
	{index = i; weight = w;}
};

class Vertex
{
	public:

	double data; //not really used in this implementation
	double coor[2]; //in this case, the (x,y) coordinates on the plane
	vector<VNode> adjacencyList;
	double getWeightByIndex(int to);
	void deleteByIndex(int to);

	Vertex(double d, double x, double y)
	{data = d; coor[0] = x; coor[1] = y;}
};


class myGraph
{
	protected:

	vector<Vertex> Vertices;
	void checkConnections(int, vector<bool>&, int&);
	vector<unsigned int> commonNeighbors(unsigned int from, unsigned int to);
	

	public:

	double getData(unsigned int i)
	{return Vertices[i].data;}

	void insertVertex(double, double, double); //with coordinates
	void insertVertex(double);

	void insertEdge(unsigned int from, unsigned int to, double w);
	void insertEdge(unsigned int from, unsigned int to);

	void deleteEdge(unsigned int from, unsigned int to);

	unsigned int getNumberVertices()
	{return Vertices.size();}

	int getConnections();
	unsigned int getEdges();
	unsigned int getMaxDegree();
	unsigned int getMinDegree();
	unsigned int getAverageDegree();

	void makeText_graphviz();
	void makeText_graphviz_Graph();
	void getStats();

};

#endif
